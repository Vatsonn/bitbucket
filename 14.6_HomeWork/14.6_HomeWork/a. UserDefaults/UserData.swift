//
//  UserData.swift
//  14.6_HomeWork
//
//  Created by Вадим Зинатуллин on 21.12.2020.
//

import Foundation

class UserData {
    
    static let shared = UserData()
    private let userNameKey = "UserData.userNameKey"
    private let userSurnameKey = "UserData.userSurnameKey"
    
    var userName : String? {
        set { UserDefaults.standard.set(newValue, forKey: userNameKey) }
        get { return UserDefaults.standard.string(forKey: userNameKey)}
    }
    
    var userSurname : String? {
        set { UserDefaults.standard.set(newValue, forKey: userSurnameKey) }
        get { return UserDefaults.standard.string(forKey: userSurnameKey)}
    }
}
