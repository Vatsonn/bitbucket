//
//  ViewController.swift
//  14.6_HomeWork
//
//  Created by Вадим Зинатуллин on 21.12.2020.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var surnameLabel: UITextField!
    @IBOutlet weak var nameLabel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        nameLabel.delegate = self
//        surnameLabel.delegate = self
        
        nameLabel.text = UserData.shared.userName
        surnameLabel.text = UserData.shared.userSurname

    }

    @IBAction func saveButton(_ sender: Any) {
        UserData.shared.userName = nameLabel.text
        UserData.shared.userSurname = surnameLabel.text
    }
    
}

//extension ViewController: UITextFieldDelegate{
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        UserData.shared.userName = nameLabel.text
//        UserData.shared.userSurname = surnameLabel.text
//
//    }
//}

