//
//  ToDoListCoreData.swift
//  14.6_HomeWork
//
//  Created by Вадим Зинатуллин on 23.12.2020.
//

import Foundation
import CoreData
import UIKit

var toDoListCoreData: [ToDoListCore] = []


func addItemCoreData(nameItem: String) {
    
    let listDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = listDelegate.persistentContainer.viewContext
    
    guard let entity = NSEntityDescription.entity(forEntityName: "ToDoListCore", in: context) else {
        return
    }
    
    let listObject = ToDoListCore(entity: entity, insertInto: context)
    listObject.name = nameItem
    listObject.isComplete = false
    
    do {
        try context.save()
        toDoListCoreData.append(listObject)
        
    } catch let error as NSError {
        print(error.localizedDescription)
    }

}

func deleteItemCoreData(at index: Int){
    toDoListCoreData.remove(at: index)
    
    let listDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = listDelegate.persistentContainer.viewContext
    
    let getData : NSFetchRequest<ToDoListCore> = ToDoListCore.fetchRequest()
    if let list = try? context.fetch(getData){
        context.delete(list[index])
    }
    do {
        try context.save()
        
    } catch let error as NSError {
        print(error.localizedDescription)
    }

}

func changeItemCoreData(at index: Int) -> Bool{
    toDoListCoreData[index].isComplete = !(toDoListCoreData[index].isComplete)

    let listDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = listDelegate.persistentContainer.viewContext
    
    let getData : NSFetchRequest<ToDoListCore> = ToDoListCore.fetchRequest()
    if let list = try? context.fetch(getData){
        context.refresh(list[index], mergeChanges: true)
    }
    do {
        try context.save()
        
    } catch let error as NSError {
        print(error.localizedDescription)
    }
    
    
    return toDoListCoreData[index].isComplete
}


