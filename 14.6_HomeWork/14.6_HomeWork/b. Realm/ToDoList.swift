//
//  ToDoList.swift
//  14.6_HomeWork
//
//  Created by Вадим Зинатуллин on 22.12.2020.
//

import Foundation
import RealmSwift

var toDoList: [[String: Any]] = []

class RealmList: Object {

    @objc dynamic var name: String = ""
    @objc dynamic var isComplete: Bool = false
}

class ToDoList {
    static let shared = ToDoList()
    private let realm = try! Realm()
    
    func addRealm() {
        let itemList = RealmList()
        itemList.name = toDoList.last!["Name"] as! String
        itemList.isComplete = toDoList.last!["isComplete"] as! Bool
        try! realm.write {
            realm.add(itemList)
        }
        
    }
    
    func dellRealm(at index: Int) {
        let dellItems = realm.objects(RealmList.self)
        
        try! realm.write {
            realm.delete(dellItems[index])
          
        }
    }
    
    func getRealm() {
        let allItems = realm.objects(RealmList.self)
        if allItems != nil {
            for item in allItems{
                toDoList.append(["Name":item.name, "isComplete": item.isComplete])
            }
        }
    }
    
    func changeItem(nameItem: String, isComplete: Bool) {
     
        let changeItem = realm.objects(RealmList.self).filter("name == %@", nameItem)
        
        try! realm.write {
            changeItem.setValue(isComplete, forKey: "isComplete")
        }


    }
    
}

func addItem(nameItem: String) {
    toDoList.append(["Name":nameItem, "isComplete": false])
    ToDoList.shared.addRealm()
}

func deleteItem(at index: Int){
    toDoList.remove(at: index)
    ToDoList.shared.dellRealm(at: index)
}

func changeItem(at index: Int) -> Bool{
    toDoList[index]["isComplete"] = !(toDoList[index]["isComplete"] as! Bool)
    ToDoList.shared.changeItem(nameItem: toDoList[index]["Name"] as! String, isComplete: toDoList[index]["isComplete"] as! Bool)
    return toDoList[index]["isComplete"] as! Bool
}


func loadList() {
    ToDoList.shared.getRealm()
}
