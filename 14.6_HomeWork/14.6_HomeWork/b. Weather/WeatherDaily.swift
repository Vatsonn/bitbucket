//
//  WeatherDaily.swift
//  14.6_HomeWork
//
//  Created by Вадим Зинатуллин on 25.12.2020.
//

import Foundation
import Alamofire
import SVProgressHUD
import MBProgressHUD

struct WeatherDailyCodable: Decodable {

    let daily: [DailyC]
    
}

struct DailyC: Decodable {
    
    let dt : Int
    let sunrise : Int
    let sunset : Int
    let temp : TempCodable
}

struct TempCodable: Decodable {
    
    let day : Double
    let min: Double
    let max: Double
    let night: Double
    let eve: Double
    let morn: Double

}

class WetherCurrentWithAlamofireCodable {

    
    func wetherDailyLoad(weatherDaily: @escaping (WeatherDailyCodable) -> Void) {

        var weatherDailyData : WeatherDailyCodable?
        
        
        
        AF.request("https://api.openweathermap.org/data/2.5/onecall?lat=59.93428&lon=30.3351&exclude=current,minutely,hourly&units=metric&appid=238c7d42e269b3cc9f94fa92187851cb").responseDecodable(of: WeatherDailyCodable.self) { response in
            
            weatherDailyData = response.value
            
            DispatchQueue.main.async {
                weatherDaily(weatherDailyData!)
            }
        }
    }
}

class WeatherCell: UITableViewCell {
    
    @IBOutlet weak var tempDayLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
}
