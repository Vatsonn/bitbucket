//
//  ViewControllerWeather.swift
//  14.6_HomeWork
//
//  Created by Вадим Зинатуллин on 25.12.2020.
//

import UIKit
import SVProgressHUD
import MBProgressHUD

class ViewControllerWeather: UIViewController {

    var weatherDailyData : WeatherDailyCodable?
    var isloadData = false

    @IBOutlet weak var weatherTableView: UITableView!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        if WeatherData.shared.weather.count == 0 {
            self.isloadData = true
        }
        
        WetherCurrentWithAlamofireCodable().wetherDailyLoad { weatherDaily in

            self.weatherDailyData = weatherDaily
            if self.weatherDailyData != nil {
            self.weatherTableView.reloadData()
            }
            MBProgressHUD.hide(for: self.view, animated: true)
        }

        
    }
    
}

extension ViewControllerWeather: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isloadData {
            return self.weatherDailyData?.daily.count ?? 0
        } else{
            return WeatherData.shared.weather.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.isloadData {
            let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as! WeatherCell
            
            let cellTemp = weatherDailyData?.daily[indexPath.row]
            
            let date = NSDate(timeIntervalSince1970: Double(cellTemp!.dt))
            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "MMM dd YYYY"
            let dateString = dayTimePeriodFormatter.string(from: date as Date)

            cell.dayLabel.text = dateString
            cell.tempDayLabel.text = String(cellTemp!.temp.day)
            
            WeatherData.shared.saveData(day: cell.dayLabel.text!, tempDay: cell.tempDayLabel.text!)
            
            return cell
        } else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as! WeatherCell
            
            let cellTemp = WeatherData.shared.weather[indexPath.row]
            cell.dayLabel.text = cellTemp.day
            cell.tempDayLabel.text = cellTemp.tempDay
            
            if indexPath.row == WeatherData.shared.weather.count-1{
                WeatherData.shared.weather.removeAll()
                self.isloadData = true
            }
            return cell
        }
    
    }
}
