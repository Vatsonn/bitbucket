//
//  WeatherData.swift
//  14.6_HomeWork
//
//  Created by Вадим Зинатуллин on 25.12.2020.
//

import Foundation

class WeatherData {
    
    let defaults = UserDefaults.standard
    static let shared = WeatherData()
    
    struct WeatherDaily: Codable {
        var tempDay : String
        var day : String
    }
    
    var weather : [WeatherDaily] {
        
        get {
            if let data = defaults.value(forKey: "weather") as? Data{
                return try! PropertyListDecoder().decode([WeatherDaily].self, from: data)
            } else {
                return [WeatherDaily]()
            }
        }
        
        set {
            if let data = try? PropertyListEncoder().encode(newValue){
                defaults.setValue(data, forKey: "weather")
            }
        }
    }
    
    func saveData(day: String, tempDay: String) {
        let weatherDay = WeatherDaily(tempDay: tempDay, day: day)
        weather.append(weatherDay)
    }
    
//    private let tempDayKey = "WeatherData.tempDayKey"
//    private let dayKey = "WeatherData.dayKey"
//
//    var tempDay : [String?] {
//        set { UserDefaults.standard.set(newValue, forKey: tempDayKey) }
//        get { return UserDefaults.standard.stringArray(forKey: tempDayKey) ?? []}
//    }
//
//    var day : [String?] {
//        set { UserDefaults.standard.set(newValue, forKey: dayKey) }
//        get { return UserDefaults.standard.stringArray(forKey: dayKey) ?? []}
//    }
    
    
}
